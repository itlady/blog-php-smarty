-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Stř 30. bře 2022, 10:36
-- Verze serveru: 10.4.22-MariaDB
-- Verze PHP: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `vos_blog`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `clanky`
--

CREATE TABLE `clanky` (
  `id` int(11) NOT NULL,
  `nazev` varchar(128) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `datum` text COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `autor` varchar(50) COLLATE utf8mb4_czech_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `clanky`
--

INSERT INTO `clanky` (`id`, `nazev`, `text`, `datum`, `autor`) VALUES
(1, 'Nejlepší triky pro Windows 11', 'Nástup systému Windows 11 byl poměrně obtížný. Přísné hardwarové požadavky, pomalé dodávky počítačů s Windows 11 a počáteční dojem, že nové funkce představují pouze kosmetické úpravy, k přechodu na novou verzi příliš nemotivovaly. Přesto je práce s novými Windows ve skutečnosti velmi příjemná a pod jejich kapotou se skrývá mnoho šikovných funkcí. Můžete například používat Windows 11 ve verzi Home i s lokálním uživatelským účtem, přizpůsobit si nabídku „Start“ podle svých preferencí a rychleji spouštět oblíbené programy.', '03. 03. 2022', 'Bill G.'),
(2, 'Uživatelé Androidu by si měli dát pozor: jeden ze skenerů QR kódu krade bankovní data', 'Patříte-li k uživatelům, kteří mají na smartphonu instalovanou čtečku QR kódu, nebo se právě po nějaké verzi rozhlížíte, buďte opatrní při své volbě. Jedna z aplikací QR čtečky instaluje malware a krade bankovní data. I přes veškerá bezpečnostní opatření je na platformě Google Play stále opakovaně k dispozici malware. V aktuálním případě se jedná o aplikaci \"QR Code & Barcode - Scanner\". Píše o tom portál Cleafy. Aplikace funguje zcela normálně a dělá to, co byste od ní očekávali. Funguje ale také jako trojský kůň pro známý malware TeaBot. Po své instalaci aplikace požádá uživatele o povolení aktualizace. Pokud jej uživatel poskytne, do smartphonu se stáhne TeaBot.\r\nTeaBot krade přístupové údaje z bankovních aplikací nebo kryptopeněženek. Za tímto účelem simuluje přihlašovací obrazovku různých služeb elektronického bankovnictví. TeaBot v současnosti dokáže napodobit více než 400 aplikací e-bankingu. Dobrou zprávou je, ža aplikace QR Code & Barcode - Scanner byla mezitím už z platformy Google Play odstraněna. Předtím si ji uživatelé stáhli v počtu 10.000. Vzhledem ke skutečnosti, že existuje mnoho neškodných aplikací, které ale mají téměř totožný název, může být komplikované zjistit, zda pokud některou z nich máte, jde o škodlivou verzi a vaše zařízení je potenciálně ohroženo. Máte-li pochybnosti, zkuste v seznamu aplikací svého mobilu vyhledat položku QR Code Scanner: Add-on. Pod tímto názvem se ukrývá TeaBot. Pokud zjistíte, že se trojský kůň TeaBot na vašem zařízení nachází, měli byste aplikaci urychleně odstranit, zkontrolovat bankovní účty a změnit přístupové údaje.', '18. 05. 2021', 'Petr'),
(10, 'Veterán z Windows už bude brzy minulostí: podpora Internet Exploreru končí v půlce června', 'Microsoft uvedl Internet Explorer na trh už v roce 1995, a to v podobě samostatné instalace. Poté se stal nedílnou součástí Windows. Lze jej stále používat i po bezmála třiceti letech, v roce 2022. Jeho konec se ale blíží. V podobě nezávislého programu zanikne a nahradí ho Microsoft Edge.\r\nV systému Windows 11 ale stále jsou pootevřená zadní vrátka. Přestože první verze Internet Exploreru byla uvolněna už před dvaceti sedmi lety, většina uživatelů jej dobře zná. Dnes ve světě prohlížečů dominuje Google se svým browserem Chrome, ale veterán IE tvrdošíjně ve Windows přetrvává a je stále využíván v některých firmách a státní správě. Brzy se ale stane minulostí. Podle informací od Microsoftu ukončí svou službu pod Windows 10 15. června 2022. Podobně jako i v minulých letech ale zbytky Internet Exploreru budou stále přetrvávat, a to i v případě Windows 11.\r\nKonec Internet Exploreru není žádným překvapením. Microsoft už své portfolio prohlížečů nějakou dobu restrukturalizuje. V podobě prohlížeče Edge uvedl v roce 2015 na trh konkurenta broswerů Chrome a Firefox, který měl v počátcích Windows 10 nahradit i vlastní prohlížeč Internet Explorer 11.\r\nPůvodní Edge se ale u uživatelů nikdy moc neujal, i když očekávání Microsoftu od nového vlastního prohlížeče bylo velké. Starší verze Edge bez základu Chromia proto už není od 9. března 2021 podporována. Místo toho byl uvolněn nový prohlížeč Microsoft Edge, který vychází z jádra Chromium. Kromě systémů Windows, Mac, Android a iOS se dočkal také verze pro Linux.', '25. 03. 2022', 'Ota Pavel'),
(12, 'Mozilla má potíže: proč se rok 2023 může stát pro Firefox osudový', 'Zdá se, že Mozilla má problém. Prohlížeč Firefox k surfování na internetu používá stále méně uživatelů a nyní také končí důležitá smlouva s Googlem. Bývalí zaměstnanci Mozilly nevěří, že by Firefox opět vstal z popela.\r\nJe to takřka přesně 20 let, kdy Mozilla uvedla na trh prohlížeč Firefox. Podle příspěvku datového analytika Kena Kovashe, který publikoval na blogu Mozilly v roce 2008, tedy šest let po uvedení Firefoxu, používalo v té době pro surfování na internetu Firefox přibližně 20 % uživatelů.\r\nOd té doby se toho ale hodně změnilo. Podle údajů statistické platformy Statcounter používá prohlížeč Firefox už několik let stále méně lidí. V únoru 2022 byl podíl uživatelů Firefoxu pouze 4,21 %, a to na všech typech koncových zařízení.\r\n\"Pokles nelze popřít\", řekla nedávno v té souvislosti Selena Deckelmannová, senior viceprezidentka Firefoxu, v rozhovoru pro technologický portál Wired. \"V posledních několika letech jsme byli svědky výrazného snížení.\" Dá se říci, že Mozilla je v útlumu.', '25. 03. 2022', 'Harry Potter'),
(14, 'Ruský notebook je přesně tak hrozný, jak si představujete', 'Letos se začne prodávat první ruský notebook Bitblaze Titan. Neohromí výkonem ani výbavou, nýbrž svou neotesaností. A stejně nakonec není tak ruský, jak se tváří. V Rusku od roku 2020 platí zákon, který nařizuje tamním úřadům a státním či obecním společnostem nakupovat určitý objem výrobků od firem z Ruska, Doněcké či Luhanské oblasti Ukrajiny nebo zemí Eurasijského ekonomického svazu.\r\nVládní nařízení dnes stanovuje 251 kategorií produktů, u nichž zmíněné firmy a úřady musí ve veřejných zakázkách od 1. ledna 2021 dávat přednost značkám z Ruska či spřátelených regionů. Týká se to všeho možného od aut a kancelářského zboží přes oděvy až po elektroniku.\r\nNapříklad u mobilů pravidla nejsou moc přísná, ruských produktů musí být letos jen 1 %. Ale třeba u „přenosných počítačů do 10 kg“, tedy notebooků a tabletů, byl loni poměr 50 %, letos již 60 % a příští rok budou muset v tendrech nakupovat 70 % domácích produktů. Zákon je napsaný dost vágně, takže stačí, aby zboží dodala firma zaregistrovaná v Rusku, Doněcku, Luhansku či EEU. Jestli komponenty pocházejí z prohnilého západu, na tom nezáleží.\r\nDlouhodobým cílem ale je přesunout výrobu do Ruska tak, aby se země stala nezávislou a odolnou vůči případným sankcím. Zákony se totiž připravovaly v době po anexi Krymu a prvních velkých restrikcích ze strany USA a EU.\r\nV Rusku existuje asi 20 „výrobců“ osobních počítačů, ti ale jen montují zahraniční hardware do sebe nebo rovnou jen přilepí své logo na OEM/ODM produkty z Číny, Tchaj-wanu či Indie. Dvě společnosti ale také navrhují vlastní procesory. MCST pracuje na čipech řady Elbrus (VLIW ISA s překladačem pro x86) a Baikal Electronics vyvíjející stejnojmenné armové procesory.', '28. 03. 2022', 'Lukáš'),
(15, 'Změna času v roce 2022: Kdy se změní letní čas na zimní', 'V roce 2022 nás stejně jako každé jaro a podzim čeká posun času o jednu hodinu. V březnu se mění čas na letní, v říjnu na standardní, který platí v zimním období. Časté označení zimní čas není správné, měli bychom říkat standardní či běžný čas. Zvláštním časem, který si zaslouží sezónní označení, je jen čas letní.\r\nLetní čas v roce 2022 začne platit v noci 27. března. Hodiny se posunou o hodinu vpřed z 2:00 na 3:00.\r\nStandardní čas (nesprávně zimní) v roce 2022 nastane v noci 30. října. Hodiny se posunou ze 3:00 na 2:00.\r\nOpravdový zimní čas, tedy sezónně posunutý v zimním období oproti standardnímu času, u nás ale rovněž krátce existoval. Bylo to v roce 1946, kdy byl tedy rozdíl mezi letním a zimním časem dvě hodiny.', '28. 03. 2022', 'David'),
(17, 'Nový robot dokáže šetrně oloupat banán. Učil se to 811 minut', 'Odborníci z University of Tokyo představili nového robota, který dokáže oloupat banán, aniž by ho poškodil. Jejich studii počátkem tohoto měsíce zveřejnil žurnál Arxiv.\r\n\r\nVe videu publikovaném minulý týden na YouTube kanálu New Scientist pak můžete vidět, jak přesně dotyčný robot při odstraňování slupky postupuje.\r\n\r\nŘadě lidí může připadat oloupání banánu jako velice jednoduchý úkol. Je však zapotřebí si uvědomit, že s činnostmi, které vyžadují jemný přístup, mají stroje obvykle velké problémy.\r\nV tomto konkrétním případě se to robot musel učit celkem 811 minut, během kterých ho tým ručně ovládal. A ani tak rozhodně nepracuje bezchybně – podle dostupných informací úspěšného výsledku dosáhne jen v o něco více než polovině případů.\r\n\r\nPotíž samozřejmě spočívá v tom, že banány lze při nešetrném zacházení snadno rozmačkat. Kromě toho, žádné dva nejsou úplně stejné (což ostatně platí i pro jiné druhy ovoce).\r\n\r\nBadatelé nicméně věří, že v budoucnosti by podobní roboti mohli pomáhat například pacientům s těžkou artritidou, pro něž mohou být velice komplikované i tak banální úkony, jako je třeba otevření zubní pasty či konzervy.', '28. 03. 2022', 'Jiří');

--
-- Indexy pro exportované tabulky
--

--
-- Indexy pro tabulku `clanky`
--
ALTER TABLE `clanky`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `clanky`
--
ALTER TABLE `clanky`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
